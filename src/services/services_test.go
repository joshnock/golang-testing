package services

import (
	"errors"
	"github.com/golang/mock/gomock"
	"mocks"
	"models"
	"testing"
	)

//Unit tests go here
//You may choose any go mocking framework you think works best for you
//Please aim to cover all the scenarios/paths in the GetItemOrSuggestions method
func Test_GivenInvalidInventoryItem_InventoryService_ShouldReturnError(t *testing.T) {
	// use the testSetUp method to mock dependencies
	mockDao, mockSuggestionService := testSetUp(t)

	// mock the expected response
	mockDao.EXPECT().GetInventoryItem(gomock.Any()).Return(nil,nil).Times(1)

	// instantiate the service under test with mocked dependencies
	invService := NewInventoryService(mockDao, mockSuggestionService)

	// execute the code we with to test
	inventoryItem, suggestions, err := invService.GetItemOrSuggestions("x", 0)

	// validate the expected responses
	if inventoryItem != nil {
		t.Fail()
	}
	if suggestions != nil {
		t.Fail()
	}
	// key test
	if err == nil {
		t.Fail()
	}
}

func Test_GivenValidInventoryItem_InventoryService_ShouldReturnInventorItem(t *testing.T) {

	// use setup() to set up the dependencies
	mockDao, mockSuggestionService := testSetUp(t)

	// this is the fake inventory item to respond with
	testItem := &models.InventoryItem{ItemId:"0", NumberInStock:10}

	// setup the mocked response
	mockDao.EXPECT().GetInventoryItem("0").Return(testItem, nil).Times(1)

	// create a new instance of the service to put under test
	invService := NewInventoryService(mockDao, mockSuggestionService)

	// execute the code under test
	inventoryItem, suggestions, err := invService.GetItemOrSuggestions("0", 10)

	// validate the expected responses
	if len(suggestions) > 0 {
		t.Fail()
	}
	if err != nil {
		t.Fail()
	}
	if inventoryItem.ItemId != testItem.ItemId {
		t.Fail()
	}
}

func Test_GivenValidInventoryItem_AndDaoError_InventoryService_ShouldReturnError(t *testing.T){

	// use setup() to set up the dependencies
	mockDao, mockSuggestionService := testSetUp(t)

	// this is the fake inventory item to respond with
	testItem := &models.InventoryItem{ItemId:"0", NumberInStock:10}

	// this is the fake error we'll return
	fakeError := errors.New("The hamsters stopped running")

	// setup the mocked response
	mockDao.EXPECT().GetInventoryItem("0").Return(testItem, fakeError).Times(1)

	// create a new instance of the service to put under test
	invService := NewInventoryService(mockDao, mockSuggestionService)

	// execute the code under test
	inventoryItem, suggestions, err := invService.GetItemOrSuggestions("0", 10)

	// validate the expected responses
	if len(suggestions) > 0{
		t.Fail()
	}
	if err != fakeError {
		t.Fail()
	}
	if inventoryItem != nil {
		t.Fail()
	}
}

func Test_GivenOutOfStockItem_InventoryService_ShouldReturnSuggestions(t *testing.T) {

	// use setup() to set up the dependencies
	mockDao, mockSuggestionService := testSetUp(t)

	// this is the fake inventory item to respond with
	testItem := &models.InventoryItem{ItemId:"0", NumberInStock:0}

	// max suggestions
	maxSuggestions := 10

	var suggestions []models.InventoryItem

	suggestions = append(suggestions, *testItem)

	// setup the mocked dao response
	mockDao.EXPECT().GetInventoryItem("0").Return(testItem, nil).Times(1)

	// setup the mocked suggestion service response
	mockSuggestionService.EXPECT().GetSuggestions("0", maxSuggestions).Return(suggestions, nil).Times(1)

	// create a new instance of the service to put under test
	invService := NewInventoryService(mockDao, mockSuggestionService)

	// execute the code under test
	inventoryItem, suggestions, err := invService.GetItemOrSuggestions("0", maxSuggestions)

	// validate the expected responses
	if suggestions == nil {
		t.Fail()
	}
	if err != nil {
		t.Fail()
	}
	if inventoryItem == nil {
		t.Fail()
	}
}

func Test_GivenOutOfStockItem_AndMaxFiveSuggestions_InventoryService_ShouldReturnFiveSuggestions(t *testing.T){
	// setup
	mockDao, mockSuggestionService := testSetUp(t)

	// set maxSuggestions to 5
	maxSuggestions := 5
	var suggestions []models.InventoryItem

	// set up five suggestions
	for i:=0; i<maxSuggestions; i++ {
		suggestions = append(suggestions, *&models.InventoryItem{ItemId:"0",NumberInStock: 3})
	}

	// setup the mocked dao response
	mockDao.EXPECT().GetInventoryItem("0").Return(&models.InventoryItem{ItemId:"0",NumberInStock: 0}, nil).Times(1)

	// setup the mocked suggestion service response
	mockSuggestionService.EXPECT().GetSuggestions("0", maxSuggestions).Return(suggestions, nil).Times(1)

	// instantiate the inventory service
	inventoryService := NewInventoryService(mockDao, mockSuggestionService)

	// execute the code under test
	inventoryItem, suggestions, err := inventoryService.GetItemOrSuggestions("0", maxSuggestions)

	// validate the expected responses
	if inventoryItem == nil {
		t.Fail()
	}
	if err != nil {
		t.Fail()
	}
	if len(suggestions) != 5 {
		t.Fail()
	}
}

func Test_GivenOutOfStockItem_AndSuggestionServiceError_InventoryService_ShouldReturnError(t *testing.T){
	// setup
	mockDao, mockSuggestionService := testSetUp(t)

	// set up the test item for the service to respond with
	testItem := &models.InventoryItem{ItemId:"0",NumberInStock: 0}

	// setup the mocked dao response
	mockDao.EXPECT().GetInventoryItem("0").Return(testItem, nil).Times(1)

	// setup the mocked suggestion service response
	mockSuggestionService.EXPECT().GetSuggestions("0", gomock.Any()).Return(nil, errors.New("no suggestions for you")).Times(1)

	// instantiate the inventory service
	invService := NewInventoryService(mockDao, mockSuggestionService)

	invItem, suggestions, err := invService.GetItemOrSuggestions("0", 200)

	// validate the expected responses
	if invItem != nil {
		t.Fail()
	}
	if suggestions != nil {
		t.Fail()
	}
	if err == nil {
		t.Fail()
	}
}

func testSetUp(t *testing.T) (*mocks.MockInventoryDao, *mocks.MockSuggestionService)  {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	// setup our two dependencies
	mockDao := mocks.NewMockInventoryDao(mockCtrl)
	mockSuggestionService := mocks.NewMockSuggestionService(mockCtrl)

	return mockDao, mockSuggestionService
}